var t = getApp();

Page({
    data: {
        statusList: [ "待付款", "待发货", "待收货", "待评价", "已完成" ],
        status: "",
        orderList: [],
        goodsMap: [],
        page: 1,
        noMore: !0
    },
    onLoad: function(t) {
        var a = this.data.page, e = t.status;
        this.setData({
            status: e
        }), this.getOrderList(a, e);
    },
    tabSelect: function(t) {
        this.setData({
            status: t.currentTarget.dataset.id,
            page: 1,
            orderList: [],
            goodsMap: [],
            noMore: !0
        });
        var a = this.data.page, e = this.data.status;
        this.getOrderList(a, e);
    },
    getOrderList: function(a, e) {
        wx.showLoading();
        var s = this;
        wx.request({
            url: t.globalData.PubUrl + "/order/list",
            method: "POST",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: {
                token: wx.getStorageSync("token"),
                page: a,
                pageSize: 10,
                status: e
            },
            success: function(t) {
                wx.hideLoading(), s.setData({
                    isshow: 1
                }), 1e4 != t.data.code && 2e3 != t.data.code ? 0 == t.data.code ? s.setData({
                    orderList: t.data.data.orderList,
                    logisticsMap: t.data.data.logisticsMap,
                    goodsMap: t.data.data.goodsMap
                }) : s.setData({
                    orderList: [],
                    logisticsMap: {},
                    goodsMap: {}
                }) : wx.navigateTo({
                    url: "/pages/login/index"
                });
            }
        });
    },
    orderDetail: function(t) {
        var a = t.currentTarget.dataset.id;
        console.log(a), wx.navigateTo({
            url: "/pages/orderDetails/index?id=" + a
        });
    },
    Go_order: function(t) {
        console.log("你是个傻帽", t.target.dataset.id);
        var a = t.target.dataset.i;
        (a = 1) || ((a = 2) ? wx.navigateTo({
            url: "/pages/hxgoods/hxgoods?id=" + a
        }) : a = 3);
    },
    onPullDownRefresh: function() {},
    onReachBottom: function() {
        if (this.data.noMore) {
            var t = this.data.page, a = this.data.status;
            this.getOrderList(t, a);
        }
    }
});