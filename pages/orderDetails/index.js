var a = getApp();

Page({
    data: {
        orderId: 0,
        goodsList: [],
        yunPrice: "0.00",
        appid: a.globalData.appid
    },
    onLoad: function(a) {
        var t = a.id;
        this.data.orderId = t, this.setData({
            orderId: t
        });
    },
    onShow: function() {
        var t = this;
        wx.request({
            url: "https://api.it120.cc/" + a.globalData.subDomain + "/order/detail",
            data: {
                token: wx.getStorageSync("token"),
                id: t.data.orderId
            },
            success: function(a) {
                wx.hideLoading(), 0 == a.data.code ? t.setData({
                    orderDetail: a.data.data
                }) : wx.showModal({
                    title: "错误",
                    content: a.data.msg,
                    showCancel: !1
                });
            }
        });
        for (var e = parseFloat(this.data.yunPrice), o = 0, r = this.data.goodsList, d = 0; d < r.length; d++) o += parseFloat(r[0].price) * r[0].number;
        this.setData({
            allGoodsPrice: o,
            yunPrice: e
        });
    },
    wuliuDetailsTap: function(a) {
        var t = a.currentTarget.dataset.id;
        wx.navigateTo({
            url: "/pages/wuliu/index?id=" + t
        });
    },
    confirmBtnTap: function(t) {
        var e = this, o = this.data.orderId, r = t.detail.formId;
        wx.showModal({
            title: "确认您已收到商品？",
            content: "",
            success: function(t) {
                t.confirm && (wx.showLoading(), wx.request({
                    url: "https://api.it120.cc/" + a.globalData.subDomain + "/order/delivery",
                    data: {
                        token: wx.getStorageSync("token"),
                        orderId: o
                    },
                    success: function(t) {
                        if (0 == t.data.code) {
                            e.onShow();
                            var d = {};
                            d.keyword1 = {
                                value: e.data.orderDetail.orderInfo.orderNumber,
                                color: "#173177"
                            };
                            var i = "您已确认收货，期待您的再次光临！";
                            a.globalData.order_reputation_score && (i += "立即好评，系统赠送您" + a.globalData.order_reputation_score + "积分奖励。"), 
                            d.keyword2 = {
                                value: i,
                                color: "#173177"
                            }, a.sendTempleMsgImmediately("uJL7D8ZWZfO29Blfq34YbuKitusY6QXxJHMuhQm_lco", r, "/pages/order-details/index?id=" + o, JSON.stringify(d));
                        }
                    }
                }));
            }
        });
    },
    submitReputation: function(t) {
        var e = this, o = t.detail.formId, r = {};
        r.token = wx.getStorageSync("token"), r.orderId = this.data.orderId;
        for (var d = [], i = 0; t.detail.value["orderGoodsId" + i]; ) {
            var s = t.detail.value["orderGoodsId" + i], n = t.detail.value["goodReputation" + i], l = t.detail.value["goodReputationRemark" + i], u = {};
            u.id = s, u.reputation = n, u.remark = l, d.push(u), i++;
        }
        r.reputations = d, wx.showLoading(), wx.request({
            url: "https://api.it120.cc/" + a.globalData.subDomain + "/order/reputation",
            data: {
                postJsonString: r
            },
            success: function(t) {
                if (wx.hideLoading(), 0 == t.data.code) {
                    e.onShow();
                    var r = {};
                    r.keyword1 = {
                        value: e.data.orderDetail.orderInfo.orderNumber,
                        color: "#173177"
                    };
                    var d = "感谢您的评价，期待您的再次光临！";
                    a.globalData.order_reputation_score && (d += a.globalData.order_reputation_score + "积分奖励已发放至您的账户。"), 
                    r.keyword2 = {
                        value: d,
                        color: "#173177"
                    }, a.sendTempleMsgImmediately("uJL7D8ZWZfO29Blfq34YbuKitusY6QXxJHMuhQm_lco", o, "/pages/order-details/index?id=" + e.data.orderId, JSON.stringify(r));
                }
            }
        });
    }
});