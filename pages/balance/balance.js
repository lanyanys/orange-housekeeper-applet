function t(t) {
    if (Array.isArray(t)) {
        for (var a = 0, e = Array(t.length); a < t.length; a++) e[a] = t[a];
        return e;
    }
    return Array.from(t);
}

var a = getApp();

Page({
    data: {
        amount: "0",
        isSwitch: !1,
        selectList: [ "全部", "收入", "支出" ],
        selectVal: "全部",
        selectIndex: "0",
        page: 1,
        behavior: "",
        billList: [],
        noMore: !0
    },
    onLoad: function(t) {
        this.setData({
            amount: t.amount
        });
        var a = this.data.page, a = this.data.page, e = this.formatTime();
        this.getAmount("", a, e);
    },
    formatTime: function() {
        var t = new Date(new Date()), a = t.getFullYear(), e = t.getMonth() + 1, o = t.getDate();
        return a + "-" + (e < 10 ? "0" + e : e) + "-" + (o < 10 ? "0" + o : o);
    },
    selectItem: function() {
        var t = this.data.isSwitch;
        this.setData({
            isSwitch: !t
        });
    },
    selectChoose: function(t) {
        console.log(t), this.getAmount(n, i);
        var a = t.currentTarget.dataset.index, e = "全部", o = "";
        console.log("index", a), 0 == a ? (e = "收支明细", o = "") : 1 == a ? (e = "收入", o = 0) : 2 == a && (e = "支出", 
        o = 1), this.setData({
            selectVal: e,
            selectIndex: a,
            page: 1,
            behavior: o,
            noMore: !0
        });
        var n = this.data.page, i = this.formatTime();
        this.getAmount(o, n, i);
    },
    getAmount: function(e, o, n) {
        wx.showLoading();
        var i = this;
        wx.request({
            url: a.globalData.PubUrl + "/user/cashLog",
            method: "POST",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: {
                token: wx.getStorageSync("token"),
                behavior: e,
                page: o,
                pageSize: "10",
                dateAddBegin: "1990-01-01",
                dateAddEnd: n
            },
            success: function(a) {
                if (wx.hideLoading(), 1 == i.data.page && i.setData({
                    billList: []
                }), 0 == a.data.code) {
                    var e = a.data.data, o = i.data.billList;
                    o = [].concat(t(o), t(e));
                    var n = i.data.page;
                    n++, i.setData({
                        billList: o,
                        page: n
                    });
                } else 700 == a.data.code ? i.setData({
                    noMore: !1
                }) : wx.showToast({
                    title: a.data.msg,
                    icon: "none"
                });
            }
        });
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {},
    onReachBottom: function() {
        if (this.data.noMore) {
            var t = this.data.behavior, a = this.data.page, e = this.formatTime();
            this.getAmount(t, a, e);
        }
    }
});