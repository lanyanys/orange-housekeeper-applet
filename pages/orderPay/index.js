var t = getApp();

Page({
    data: {
        id: "",
        isNeedLogistics: 0,
        goodsList: [],
        orderInfo: {},
        orderType: "",
        logs: {},
        mobile: "",
        formId: "",
        tel: "",
        allGoodsPrice: 0,
        yunPrice: 0,
        peisongType: "kd",
        allGoodsAndYunPrice: 0
    },
    onShow: function() {
        var t = this, a = [];
        if ("buyNow" == t.data.orderType) {
            var e = wx.getStorageSync("buyNowInfo");
            t.data.kjId = e.kjId, e && e.shopList && (a = e.shopList);
        } else {
            var o = wx.getStorageSync("shopCarInfo");
            t.data.kjId = o.kjId, o && o.shopList && (a = o.shopList.filter(function(t) {
                return t.active;
            }));
        }
        t.setData({
            goodsList: a
        }), t.initShippingAddress();
    },
    onLoad: function(t) {
        wx.hideLoading(), this.setData({
            isNeedLogistics: 1,
            orderType: t.orderType,
            tel: wx.getStorageSync("tel")
        });
    },
    getTatil: function(a) {
        var e = this;
        wx.request({
            url: t.globalData.PubUrl + "order/detail",
            data: {
                id: a,
                token: wx.getStorageSync("token")
            },
            success: function(t) {
                console.log(t.data.data), 0 == t.data.code && e.setData({
                    good: t.data.data.goods[0],
                    orderInfo: t.data.data.orderInfo,
                    logs: t.data.data.logs[0]
                });
            },
            error: function() {}
        });
    },
    phoneInput: function(t) {
        console.log(t.detail.value), this.setData({
            mobile: t.detail.value
        });
    },
    yzmobile: function(t) {
        return 0 == t.length ? (wx.showToast({
            title: "请输入手机号！",
            icon: "none",
            duration: 1e3
        }), !1) : 11 != t.length ? (wx.showToast({
            title: "手机号长度有误！",
            icon: "none",
            duration: 1e3
        }), !1) : !!/^1[3456789]\d{9}$/.test(t) || (wx.showToast({
            title: "手机号有误！",
            icon: "none",
            duration: 1e3
        }), !1);
    },
    pay: function(a) {
        var e = this.data.logs.orderId, o = this.data.orderInfo.amountReal, d = a.detail.value.mobile, s = {};
        return this.data.isNeedLogistics > 0 && !this.data.curAddressData ? (wx.showToast({
            title: "请先设置您的收货地址",
            icon: "none"
        }), !1) : 0 != this.yzmobile(d) && (wx.setStorageSync("tel", d), 0 != e && ("支付订单 ：" + e, 
        s = {
            type: 0,
            id: e
        }), void wx.request({
            url: t.globalData.PubUrl + "pay/wx/wxapp",
            method: "POST",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: {
                token: wx.getStorageSync("token"),
                money: o,
                remark: d,
                payName: "在线支付",
                nextAction: JSON.stringify(s)
            },
            success: function(t) {
                0 == t.data.code ? wx.requestPayment({
                    timeStamp: t.data.data.timeStamp,
                    nonceStr: t.data.data.nonceStr,
                    package: "prepay_id=" + t.data.data.prepayId,
                    signType: "MD5",
                    paySign: t.data.data.sign,
                    fail: function(t) {
                        wx.showToast({
                            title: "支付失败:" + t
                        });
                    },
                    success: function(t) {
                        wx.showToast({
                            title: "支付成功"
                        }), wx.navigateTo({
                            url: "/pages/orderList/orderList?satsus=2"
                        });
                    }
                }) : wx.showToast({
                    title: "服务器忙" + t.data.code + t.data.msg
                });
            }
        }));
    },
    initShippingAddress: function() {
        var a = this;
        console.log("获取地址"), wx.request({
            url: t.globalData.PubUrl + "user/shipping-address/default",
            data: {
                token: wx.getStorageSync("token")
            },
            success: function(t) {
                console.log(t.data), 0 == t.data.code ? a.setData({
                    curAddressData: t.data.data
                }) : a.setData({
                    curAddressData: null
                }), a.processYunfei();
            },
            fail: function() {
                console.log("1111");
            }
        });
    },
    processYunfei: function() {
        var t = this, a = this.data.goodsList, e = "[", o = 0, d = 0, s = wx.getStorageSync("referrer");
        s && (d = s);
        for (var i = 0; i < a.length; i++) {
            var r = a[i];
            console.log(r), r.logistics && (o = 1), r.price * r.number;
            var n = "";
            i > 0 && (n = ","), e += n += '{"goodsId":' + r.goodsId + ',"number":' + r.number + ',"propertyChildIds":"' + r.propertyChildIds + '","logisticsType":0, "inviter_id":' + d + "}";
        }
        e += "]", t.setData({
            isNeedLogistics: o,
            goodsJsonStr: e
        }), t.createOrder();
    },
    addAddress: function() {
        wx.navigateTo({
            url: "/pages/address/list"
        });
    },
    selectAddress: function() {
        wx.navigateTo({
            url: "/pages/address/list"
        });
    },
    createOrder: function(a) {
        var e = this;
        wx.getStorageSync("token");
        a && a.detail.value.remark;
        var o = {
            token: wx.getStorageSync("token"),
            remark: wx.getStorageSync("tel"),
            expireMinutes: 30,
            goodsJsonStr: e.data.goodsJsonStr
        };
        if (this.data.isNeedLogistics ? o.isCanHx = !1 : o.isCanHx = !0, e.data.isNeedLogistics > 0) {
            if (!e.data.curAddressData) return wx.hideLoading(), void wx.showModal({
                title: "错误",
                content: "请先设置您的收货地址！",
                showCancel: !1
            });
            o.provinceId = e.data.curAddressData.provinceId, o.cityId = e.data.curAddressData.cityId, 
            e.data.curAddressData.districtId && (o.districtId = e.data.curAddressData.districtId), 
            o.address = e.data.curAddressData.address, o.linkMan = e.data.curAddressData.linkMan, 
            o.mobile = e.data.curAddressData.mobile, o.code = " ";
        }
        e.data.curCoupon && (o.couponId = e.data.curCoupon.id), wx.request({
            url: t.globalData.PubUrl + "order/create",
            method: "POST",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            data: o,
            success: function(t) {
                console.log(t), 0 == t.data.code ? (e.getTatil(t.data.data.id), a && "buyNow" != e.data.orderType && wx.removeStorageSync("shopCarInfo"), 
                a || e.setData({
                    totalScoreToPay: t.data.data.score,
                    isNeedLogistics: t.data.data.isNeedLogistics,
                    allGoodsPrice: t.data.data.amount,
                    allGoodsAndYunPrice: t.data.data.amountLogistics + t.data.data.amountTotle,
                    yunPrice: t.data.data.amountLogistics
                })) : wx.showModal({
                    title: "错误",
                    content: t.data.msg,
                    showCancel: !1
                });
            }
        });
    }
});