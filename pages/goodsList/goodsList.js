var t = getApp();

Page({
    data: {
        page: 1,
        categoryId: "",
        goodsList: []
    },
    onLoad: function(t) {
        this.setData({
            categoryId: t.id
        }), this.requestList(1);
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    onPullDownRefresh: function() {
        wx.showNavigationBarLoading(), this.setData({
            page: 1,
            goodsList: []
        }), this.requestList(1), wx.stopPullDownRefresh(), wx.hideNavigationBarLoading();
    },
    onReachBottom: function() {
        this.requestList(this.data.page);
    },
    onShareAppMessage: function() {},
    buyClick: function(t) {
        var a = t.currentTarget.dataset.goodsid, o = t.currentTarget.dataset.shopid;
        wx.navigateTo({
            url: "/pages/goodsDetail/index?id=" + a + "&shopId=" + o
        });
    },
    requestList: function(a) {
        var o = this;
        wx.showLoading(), wx.request({
            url: t.globalData.PubUrl + "shop/goods/list",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            method: "POST",
            data: {
                page: a,
                categoryId: o.data.categoryId
            },
            success: function(t) {
                if (wx.hideLoading(), 0 == t.data.code) {
                    var a = o.data.goodsList.concat(t.data.data);
                    o.setData({
                        goodsList: a,
                        page: o.data.page + 1
                    });
                } else 700 == t.data.code && wx.showToast({
                    icon: "none",
                    title: "没有更多数据"
                });
            },
            fail: function(t) {
                wx.hideLoading();
            }
        });
    }
});