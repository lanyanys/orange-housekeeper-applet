var t = getApp();

Page({
    data: {
        extinfo: ""
    },
    onLoad: function(t) {
        var a = t.key;
        this._getext(a);
    },
    _getext: function(a) {
        var e = this;
        wx.request({
            url: t.globalData.PubUrl + "/notice/detail",
            data: {
                id: a
            },
            success: function(t) {
                console.log(t.data.data), e.setData({
                    extinfo: t.data.data.content
                }), wx.setNavigationBarTitle({
                    title: t.data.data.title
                });
            }
        });
    },
    onShareAppMessage: function() {}
});