var e = getApp();

Page({
    data: {
        mallName: ""
    },
    onLoad: function(e) {
        this.setData({
            mallName: wx.getStorageSync("mallName")
        });
    },
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {},
    cancel_login: function() {
        wx.navigateBack();
    },
    bindGetUserInfo: function(e) {
        e.detail.userInfo && (wx.setStorageSync("userInfo", e.detail.userInfo), this.login());
    },
    login: function() {
        var a = this, t = wx.getStorageSync("token");
        t ? wx.request({
            url: e.globalData.PubUrl + "/user/check-token",
            data: {
                token: t
            },
            success: function(e) {
                console.log(e.data), 0 != e.data.code ? (wx.removeStorageSync("token"), a.login()) : wx.navigateBack();
            }
        }) : wx.login({
            success: function(t) {
                wx.request({
                    url: e.globalData.PubUrl + "/user/wxapp/login",
                    data: {
                        code: t.code
                    },
                    success: function(e) {
                        if (1e4 != e.data.code) {
                            if (0 != e.data.code) return wx.hideLoading(), void wx.showModal({
                                title: "提示",
                                content: "无法登录，请重试",
                                showCancel: !1
                            });
                            wx.setStorageSync("token", e.data.data.token), wx.setStorageSync("uid", e.data.data.uid), 
                            wx.navigateBack();
                        } else a.registerUser();
                    }
                });
            }
        });
    },
    registerUser: function() {
        var a = this;
        wx.login({
            success: function(t) {
                var n = t.code;
                wx.getUserInfo({
                    success: function(t) {
                        var o = t.iv, c = t.encryptedData;
                        wx.request({
                            url: e.globalData.PubUrl + "/user/wxapp/register/complex",
                            data: {
                                code: n,
                                encryptedData: c,
                                iv: o
                            },
                            success: function(e) {
                                wx.hideLoading(), a.login();
                            }
                        });
                    }
                });
            }
        });
    }
});