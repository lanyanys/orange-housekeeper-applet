var t = getApp();

Page({
    data: {
        showLogin: 0,
        userInfo: {},
        balance: "",
        orderStatus: [ {
            icon: "/static/svg/icon-pay.svg",
            name: "待付款",
            path: "../orderList/orderList?status=0"
        }, {
            icon: "/static/svg/icon-use.svg",
            name: "待发货",
            path: "../orderList/orderList?status=1"
        }, {
            icon: "/static/svg/icon-ship.svg",
            name: "待收货",
            path: "../orderList/orderList?status=2"
        }, {
            icon: "/static/svg/icon-reply.svg",
            name: "待评价",
            path: "../orderList/orderList?status=3"
        } ],
        ordercat: [ {
            icon: "/static/svg/icon-xieyi.svg",
            name: "地址管理",
            path: "/pages/address/list",
            type: 0
        }, {
            icon: "/static/svg/icon-kefu.svg",
            name: "联系客服",
            path: "",
            type: 1
        }, {
            icon: "/static/svg/icon-xieyi.svg",
            name: "服务协议",
            path: "/pages/extpage/index?key=17473",
            type: 0
        }, {
            icon: "/static/svg/icon-gree.svg",
            name: "隐私政策",
            path: "/pages/extpage/index?key=17474",
            type: 0
        } ]
    },
    onLoad: function(t) {
        wx.setBackgroundColor({
            backgroundColorTop: "#29bf76",
            backgroundColorBottom: "#ffffff"
        });
        var a = t.msg;
        console.log(a), a && wx.showModal({
            title: "请先登录",
            content: ""
        });
    },
    getAmount: function() {
        wx.showLoading();
        var a = this;
        wx.request({
            url: t.globalData.PubUrl + "/user/amount",
            data: {
                token: wx.getStorageSync("token")
            },
            success: function(t) {
                wx.hideLoading(), 0 == t.data.code ? a.setData({
                    balance: t.data.data.balance
                }) : wx.showToast({
                    title: t.data.ext,
                    icon: "none"
                });
            }
        });
    },
    wx_login: function() {
        wx.navigateTo({
            url: "/pages/login/index"
        });
    },
    goBalance: function(t) {
        console.log(t.currentTarget.dataset), wx.navigateTo({
            url: "/pages/balance/balance?amount=" + t.currentTarget.dataset.amount
        });
    },
    gotab: function(t) {
        var a = t.target.dataset.url;
        wx.navigateTo({
            url: a
        });
    },
    logoout: function() {
        wx.clearStorageSync("token", ""), wx.clearStorageSync("uid", ""), wx.clearStorageSync("userInfo", ""), 
        this.setData({
            userInfo: ""
        });
    },
    onReady: function() {},
    onShow: function() {
        var a = this;
        t.isLogin(wx.getStorageSync("token"));
        var o = wx.getStorageSync("userInfo");
        o && a.setData({
            userInfo: o
        });
    },
    GoOrder: function(t) {
        var a = t.currentTarget.dataset.url;
        wx.navigateTo({
            url: a
        });
    },
    onPullDownRefresh: function() {
        wx.showNavigationBarLoading();
        var t = wx.getStorageSync("token"), a = wx.getStorageSync("userInfo");
        t && this.getAmount(), this.setData({
            userInfo: a
        }), wx.stopPullDownRefresh(), wx.hideNavigationBarLoading();
    },
    onReachBottom: function() {}
});