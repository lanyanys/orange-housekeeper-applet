var t = getApp(), e = require("../../utils/city.js");

Page(function(t, e, a) {
    return e in t ? Object.defineProperty(t, e, {
        value: a,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : t[e] = a, t;
}({
    data: {
        provinces: [],
        citys: [],
        districts: [],
        selProvince: "请选择",
        selCity: "请选择",
        selDistrict: "请选择",
        selProvinceIndex: 0,
        selCityIndex: 0,
        selDistrictIndex: 0,
        region: [ "河南省", "驻马店市", "泌阳县" ],
        code: [ "410000", "411700", "411726" ],
        postcode: "463700"
    },
    onLoad: function(e) {
        var a = this;
        this.initCityData(1);
        var i = e.id;
        i && (wx.showLoading(), wx.request({
            url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/detail",
            data: {
                token: wx.getStorageSync("token"),
                id: i
            },
            success: function(t) {
                if (wx.hideLoading(), 0 == t.data.code) return a.setData({
                    id: i,
                    addressData: t.data.data,
                    region: [ t.data.data.provinceStr, t.data.data.cityStr, t.data.data.areaStr ],
                    selProvince: t.data.data.provinceStr,
                    selCity: t.data.data.cityStr,
                    selDistrict: t.data.data.areaStr
                }), void a.setDBSaveAddressId(t.data.data);
                wx.showModal({
                    title: "提示",
                    content: "无法获取快递地址数据",
                    showCancel: !1
                });
            }
        }));
    },
    deleteAddress: function(e) {
        var a = e.currentTarget.dataset.id;
        console.log(a), wx.showModal({
            title: "提示",
            content: "确定要删除该收货地址吗？",
            success: function(e) {
                e.confirm ? wx.request({
                    url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/delete",
                    data: {
                        token: wx.getStorageSync("token"),
                        id: a
                    },
                    success: function(t) {
                        wx.navigateBack({});
                    }
                }) : e.cancel && console.log("用户点击取消");
            }
        });
    },
    RegionChange: function(t) {
        console.log(t), this.setData({
            region: t.detail.value,
            code: t.detail.code,
            postcode: t.detail.postcode
        });
    },
    onShow: function() {},
    bindSave: function(e) {
        var a = this, i = e.detail.value.linkMan, s = e.detail.value.address, n = e.detail.value.mobile, d = a.data.postcode, c = a.data.code;
        if ("" != i) if ("" != n) if ("" != s) {
            var o = "add", l = a.data.id;
            l ? o = "update" : l = 0, wx.request({
                url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/" + o,
                data: {
                    token: wx.getStorageSync("token"),
                    id: l,
                    provinceId: c[0],
                    cityId: c[1],
                    districtId: c[2],
                    linkMan: i,
                    address: s,
                    mobile: n,
                    code: d,
                    isDefault: "true"
                },
                success: function(t) {
                    if (0 != t.data.code) return wx.hideLoading(), void wx.showModal({
                        title: "失败",
                        content: t.data.msg,
                        showCancel: !1
                    });
                    wx.navigateBack({});
                }
            });
        } else wx.showModal({
            title: "提示",
            content: "请填写详细地址",
            showCancel: !1
        }); else wx.showModal({
            title: "提示",
            content: "请填写手机号码",
            showCancel: !1
        }); else wx.showModal({
            title: "提示",
            content: "请填写联系人姓名",
            showCancel: !1
        });
    },
    initCityData: function(t, a) {
        if (1 == t) {
            for (var i = [], s = 0; s < e.cityData.length; s++) i.push(e.cityData[s].name);
            this.setData({
                provinces: i
            });
        } else if (2 == t) {
            for (var i = [], n = a.cityList, s = 0; s < n.length; s++) i.push(n[s].name);
            this.setData({
                citys: i
            });
        } else if (3 == t) {
            for (var i = [], n = a.districtList, s = 0; s < n.length; s++) i.push(n[s].name);
            this.setData({
                districts: i
            });
        }
    },
    bindPickerProvinceChange: function(t) {
        var a = e.cityData[t.detail.value];
        this.setData({
            selProvince: a.name,
            selProvinceIndex: t.detail.value,
            selCity: "请选择",
            selCityIndex: 0,
            selDistrict: "请选择",
            selDistrictIndex: 0
        }), this.initCityData(2, a);
    },
    bindPickerCityChange: function(t) {
        var a = e.cityData[this.data.selProvinceIndex].cityList[t.detail.value];
        this.setData({
            selCity: a.name,
            selCityIndex: t.detail.value,
            selDistrict: "请选择",
            selDistrictIndex: 0
        }), this.initCityData(3, a);
    },
    bindPickerChange: function(t) {
        var a = e.cityData[this.data.selProvinceIndex].cityList[this.data.selCityIndex].districtList[t.detail.value];
        a && a.name && t.detail.value && this.setData({
            selDistrict: a.name,
            selDistrictIndex: t.detail.value
        });
    },
    setDBSaveAddressId: function(t) {
        for (var a = 0; a < e.cityData.length; a++) if (t.provinceId == e.cityData[a].id) {
            this.data.selProvinceIndex = a;
            for (var i = 0; i < e.cityData[a].cityList.length; i++) if (t.cityId == e.cityData[a].cityList[i].id) {
                this.data.selCityIndex = i;
                for (var s = 0; s < e.cityData[a].cityList[i].districtList.length; s++) t.districtId == e.cityData[a].cityList[i].districtList[s].id && (this.data.selDistrictIndex = s);
            }
        }
    },
    selectCity: function() {}
}, "deleteAddress", function(e) {
    var a = e.currentTarget.dataset.id;
    wx.showModal({
        title: "提示",
        content: "确定要删除该收货地址吗？",
        success: function(e) {
            e.confirm ? wx.request({
                url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/delete",
                data: {
                    token: wx.getStorageSync("token"),
                    id: a
                },
                success: function(t) {
                    wx.navigateBack({});
                }
            }) : e.cancel && console.log("用户点击取消");
        }
    });
}));