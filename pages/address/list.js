var t = getApp(), i = require("../../utils/city.js");

Page({
    data: {
        addressList: [],
        selProvince: "请选择",
        selCity: "请选择",
        selDistrict: "请选择",
        selProvinceIndex: 0,
        selCityIndex: 0,
        selDistrictIndex: 0
    },
    onLoad: function(t) {},
    selectTap: function(i) {
        var e = i.currentTarget.dataset.id;
        console.log(e), wx.request({
            url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/update",
            data: {
                token: wx.getStorageSync("token"),
                id: e,
                isDefault: "true"
            },
            success: function(t) {
                wx.navigateBack({});
            }
        });
    },
    editAddress: function(t) {
        var i = t.currentTarget.dataset.id;
        console.log(i), wx.navigateTo({
            url: "/pages/address/detail?id=" + i
        });
    },
    navAddAddress: function() {
        wx.navigateTo({
            url: "/pages/address/detail"
        });
    },
    onShow: function() {
        t.isLogin(wx.getStorageSync("token")), this.initShippingAddress();
    },
    initShippingAddress: function() {
        var i = this;
        wx.request({
            url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/list",
            data: {
                token: wx.getStorageSync("token")
            },
            success: function(t) {
                0 == t.data.code ? i.setData({
                    addressList: t.data.data
                }) : 700 == t.data.code && i.setData({
                    addressList: null
                });
            }
        });
    },
    importAddress: function() {
        var e = this;
        console.log(111), wx.chooseAddress({
            success: function(a) {
                for (var s = a.provinceName, n = a.cityName, d = a.countyName, c = 0; c < i.cityData.length; c++) if (s == i.cityData[c].name) {
                    var r = {
                        detail: {
                            value: c
                        }
                    };
                    e.bindPickerProvinceChange(r), e.data.selProvinceIndex = c;
                    for (var l = 0; l < i.cityData[c].cityList.length; l++) if (n == i.cityData[c].cityList[l].name) {
                        r = {
                            detail: {
                                value: l
                            }
                        }, e.bindPickerCityChange(r);
                        for (var o = 0; o < i.cityData[c].cityList[l].districtList.length; o++) d == i.cityData[c].cityList[l].districtList[o].name && (r = {
                            detail: {
                                value: o
                            }
                        }, e.bindPickerChange(r));
                    }
                }
                console.log(a), e.setData({
                    wxaddress: a
                });
                var u, v = i.cityData[e.data.selProvinceIndex].cityList[e.data.selCityIndex].id;
                u = "请选择" != e.data.selDistrict && e.data.selDistrict ? i.cityData[e.data.selProvinceIndex].cityList[e.data.selCityIndex].districtList[e.data.selDistrictIndex].id : "", 
                wx.request({
                    url: "https://api.it120.cc/" + t.globalData.subDomain + "/user/shipping-address/add",
                    data: {
                        token: wx.getStorageSync("token"),
                        id: 0,
                        provinceId: i.cityData[e.data.selProvinceIndex].id,
                        cityId: v,
                        districtId: u,
                        linkMan: a.userName,
                        address: a.detailInfo,
                        mobile: a.telNumber,
                        code: a.postalCode,
                        isDefault: "true"
                    },
                    success: function(t) {
                        if (0 != t.data.code) return wx.hideLoading(), void wx.showModal({
                            title: "失败",
                            content: t.data.msg,
                            showCancel: !1
                        });
                        wx.navigateBack({});
                    }
                });
            }
        });
    },
    bindPickerCityChange: function(t) {
        var e = i.cityData[this.data.selProvinceIndex].cityList[t.detail.value];
        this.setData({
            selCity: e.name,
            selCityIndex: t.detail.value,
            selDistrict: "请选择",
            selDistrictIndex: 0
        }), this.initCityData(3, e);
    },
    bindPickerProvinceChange: function(t) {
        var e = i.cityData[t.detail.value];
        this.setData({
            selProvince: e.name,
            selProvinceIndex: t.detail.value,
            selCity: "请选择",
            selCityIndex: 0,
            selDistrict: "请选择",
            selDistrictIndex: 0
        }), this.initCityData(2, e);
    },
    initCityData: function(t, e) {
        if (1 == t) {
            for (var a = [], s = 0; s < i.cityData.length; s++) a.push(i.cityData[s].name);
            this.setData({
                provinces: a
            });
        } else if (2 == t) {
            for (var a = [], n = e.cityList, s = 0; s < n.length; s++) a.push(n[s].name);
            this.setData({
                citys: a
            });
        } else if (3 == t) {
            for (var a = [], n = e.districtList, s = 0; s < n.length; s++) a.push(n[s].name);
            this.setData({
                districts: a
            });
        }
    },
    bindPickerChange: function(t) {
        var e = i.cityData[this.data.selProvinceIndex].cityList[this.data.selCityIndex].districtList[t.detail.value];
        e && e.name && t.detail.value && this.setData({
            selDistrict: e.name,
            selDistrictIndex: t.detail.value
        });
    }
});