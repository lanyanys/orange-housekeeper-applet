Page({
    data: {
        c_list: [ {
            avatar: "/static/img/33.jpg",
            nickname: "晨晨",
            wx: "18838515218",
            label: [ "在线客服", "在线问答" ],
            score: "A",
            number: "10",
            grade: "A"
        }, {
            avatar: "/static/img/22.jpg",
            nickname: "静静",
            wx: "18838515218",
            label: [ "在线客服", "在线问答" ],
            score: "A",
            number: "10",
            grade: "A"
        } ]
    },
    onLoad: function(a) {},
    copyText: function(a) {
        var t = a.target.dataset.wx;
        console.log(t), wx.setClipboardData({
            data: t,
            success: function(a) {}
        });
    },
    onShareAppMessage: function() {}
});