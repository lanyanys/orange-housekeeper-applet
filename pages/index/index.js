var a = getApp();

Page({
    data: {
        swiperList: [],
        gridCol: 4,
        searchVal: "",
        categoryList: [],
        goodsList: [],
        page: 1
    },
    onLoad: function (a) {
        var t = this;
        wx.getStorageSync("mallName") && wx.setNavigationBarTitle({
            title: wx.getStorageSync("mallName")
        }), t.requestBanner(), t.requestCategory(), t.requestProducts(t.data.page);
    },
    requestBanner: function () {
        var t = this;
        wx.showLoading(), wx.request({
            url: a.globalData.PubUrl + "banner/list",
            success: function (a) {
                console.log("轮播", a.data.data), wx.hideLoading(), t.setData({
                    swiperList: a.data.data
                });
            },
            fail: function (a) {
                wx.hideLoading();
            }
        });
    },
    requestCategory: function () {
        var t = this;
        wx.showLoading(), wx.request({
            url: a.globalData.PubUrl + "shop/goods/category/all",
            method: "POST",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            success: function (a) {
                console.log("分类", a.data.data), wx.hideLoading(), 0 == a.data.code && t.setData({
                    categoryList: a.data.data
                });
            },
            fail: function (a) {
                wx.hideLoading();
            }
        });
    },
    icoClick: function (a) {
        var t = a.currentTarget.dataset.id;
        wx.navigateTo({
            url: "/pages/goodsList/goodsList?id=" + t
        });
    },
    requestProducts: function (t) {
        var o = this;
        console.log("请求方法中，page=", o.data.page), wx.showLoading(), wx.request({
            url: a.globalData.PubUrl + "shop/goods/list",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            method: "POST",
            data: {
                page: t,
                pageSize: 4
            },
            success: function (a) {
                if (wx.hideLoading(), 0 == a.data.code) {
                    var t = o.data.goodsList.concat(a.data.data);
                    o.setData({
                        goodsList: t,
                        page: o.data.page + 1
                    });
                } else 700 == a.data.code ? wx.showToast({
                    icon: "none",
                    title: "没有更多数据"
                }) : console.log("请求数据异常");
            },
            fail: function (a) {
                wx.hideLoading(), console.log("网络交互失败");
            }
        });
    },
    onPullDownRefresh: function () {
        var a = this;
        wx.showNavigationBarLoading(), a.setData({
            page: 1,
            goodsList: []
        }), a.requestCategory(), a.requestProducts(1), wx.stopPullDownRefresh(), wx.hideNavigationBarLoading();
    },
    bindputsearch: function (a) {
        console.log(a.detail.value), this.setData({
            searchVal: a.detail.value
        });
    },
    Tapsearch: function () {
        var t = this,
            o = t.data.searchVal;
        console.log(o), "" != o && (wx.showLoading(), wx.request({
            url: a.globalData.PubUrl + "shop/goods/list",
            header: {
                "content-type": "application/x-www-form-urlencoded"
            },
            method: "POST",
            data: {
                k: o
            },
            success: function (a) {
                wx.hideLoading(), 0 == a.data.code ? t.setData({
                    goodsList: a.data.data
                }) : 700 == a.data.code ? wx.showToast({
                    icon: "none",
                    title: "没有更多数据"
                }) : console.log("请求数据异常");
            },
            fail: function (a) {
                wx.hideLoading(), console.log("网络交互失败");
            }
        }));
    },
    onReachBottom: function () {
        var a = this;
        a.requestProducts(a.data.page);
    },
    goodsClick: function (a) {
        console.log(a);
        var t = a.currentTarget.dataset.goodsid,
            o = a.currentTarget.dataset.shopid;
        wx.navigateTo({
            url: "/pages/goodsDetail/index?id=" + t + "&shopId=" + o
        });
    },
    onShareAppMessage: function () {}
});