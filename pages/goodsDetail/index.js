function t(t, a, e) {
    return a in t ? Object.defineProperty(t, a, {
        value: e,
        enumerable: !0,
        configurable: !0,
        writable: !0
    }) : t[a] = e, t;
}

var a, e = require("../../utils/card"), o = getApp().globalData, i = getApp();

Page({
    data: (a = {
        isshow: !0,
        id: "",
        shopNum: 0,
        top: 20,
        collect: 0,
        shouc: 0,
        isShow: 0,
        taboption: 1,
        shopTatil: {},
        DetailsData: {},
        shangjiaData: {},
        detailsTitleData: [ "商品详情", "客户评价" ],
        daojishi: 0,
        currentTabsIndex: 0,
        actionSheetHidden: !0,
        showPosterBox: !1,
        start: !1,
        payshow: !1,
        imgheights: [],
        current: 0,
        goodsDetail: {},
        goodsDetailDefault: {},
        selectSizePrice: 0,
        hasMoreSelect: !1,
        shopCarInfo: {},
        selectSize: "选择："
    }, t(a, "selectSizePrice", 0), t(a, "totalScoreToPay", 0), t(a, "hideShopPopup", !0), 
    t(a, "buyNumber", 0), t(a, "buyNumMin", 1), t(a, "buyNumMax", 0), t(a, "shopType", "tobuy"), 
    t(a, "canSubmit", !1), t(a, "barCode", ""), t(a, "propertyChildIds", ""), t(a, "propertyChildNames", ""), 
    a),
    onLoad: function(t) {
        var a = this, e = t.id || t.scene;
        a.setData({
            id: e
        }), a._getTatil(), a.reputation(a.data.id), wx.getStorage({
            key: "shopCarInfo",
            success: function(t) {
                a.setData({
                    shopCarInfo: t.data,
                    shopNum: t.data.shopNum
                });
            }
        });
    },
    onShow: function() {
        i.isLogin(wx.getStorageSync("token"));
    },
    goShopCar: function() {
        wx.reLaunch({
            url: "/pages/shopcar/index"
        });
    },
    _getTatil: function() {
        var t = this;
        wx.request({
            url: o.PubUrl + "shop/goods/detail",
            data: {
                id: this.data.id
            },
            success: function(a) {
                if (0 == a.data.code) {
                    console.log("--商品详情输出--", a.data.data);
                    var e = a.data.data;
                    if (e.properties) {
                        for (var o = "", i = 0; i < e.properties.length; i++) o = o + " " + e.properties[i].name;
                        t.setData({
                            hasMoreSelect: !0,
                            selectSize: t.data.selectSize + o,
                            selectSizePrice: e.basicInfo.minPrice
                        });
                    }
                    var s = {
                        id: e.basicInfo.id,
                        barCode: e.basicInfo.barCode || "",
                        goodsDetail: e,
                        goodsDetailDefault: e,
                        selectSizePrice: e.basicInfo.minPrice,
                        totalScoreToPay: e.basicInfo.minScore,
                        buyNumMax: e.basicInfo.stores,
                        buyNumber: e.basicInfo.stores > 0 ? 1 : 0
                    };
                    t.setData(s), t.setData({
                        shopTatil: a.data.data,
                        DetailsData: a.data.data.basicInfo,
                        isShow: 1
                    }), a.data.data.basicInfo.characteristic && t.decode(a.data.data.basicInfo.characteristic), 
                    t._getShangjia(a.data.data.basicInfo.shopId);
                }
            },
            error: function() {
                t.setData({
                    isShow: 1
                });
            }
        });
    },
    decode: function(t) {
        var a = t.split("\n"), e = [];
        for (var o in a) e = e.concat(a[o]), console.log(e);
        this.setData({
            need_know: e
        });
    },
    play_rulopen: function() {
        wx.navigateTo({
            url: "/pages/play_rul/play_rul"
        });
    },
    shopcoll: function() {
        var t = this;
        wx.request({
            url: o.PubUrl + "/shop/goods/fav/check",
            data: {
                token: wx.getStorageSync("token"),
                goodsId: t.data.id
            },
            success: function(a) {
                console.log("shoucang", a.data), 0 == a.data.code && t.setData({
                    collect: 1,
                    shouc: 1
                });
            }
        });
    },
    inputWacth: function(t) {
        console.log(t);
        var a = t.detail.value;
        if (a >= this.data.buyNumMax) return wx.showToast({
            title: "已经最大库存数啦",
            icon: "none",
            duration: 2e3
        }), void this.setData({
            buyNumber: this.data.buyNumMax
        });
        this.setData({
            buyNumber: a,
            finallyPrice: (a * this.data.price).toFixed(2)
        });
    },
    reputation: function(t) {
        var a = this;
        wx.request({
            url: o.PubUrl + "/shop/goods/reputation",
            data: {
                goodsId: t
            },
            success: function(t) {
                0 == t.data.code && (console.log("评论", t.data.data), a.setData({
                    reputation: t.data.data
                }));
            }
        });
    },
    _getShangjia: function(t) {
        var a = this;
        console.log(t), wx.request({
            url: o.PubUrl + "shop/subshop/detail",
            data: {
                id: t
            },
            success: function(t) {
                a.setData({
                    shangjiaData: t.data.data
                }), console.log("商家数据", t.data.data);
            },
            error: function() {}
        });
    },
    Share: function() {
        this.setData({
            actionSheetHidden: !1
        });
    },
    closeShareBox: function() {
        this.setData({
            actionSheetHidden: !0
        });
    },
    bindGuiGeTap: function() {
        this.setData({
            hideShopPopup: !1
        });
    },
    closePopupTap: function() {
        this.setData({
            hideShopPopup: !0,
            goodsDetail: this.data.goodsDetailDefault,
            buyNumber: this.data.goodsDetailDefault.basicInfo.stores > 0 ? 1 : 0,
            canSubmit: !1
        });
    },
    changebap: function(t) {
        var a = this, e = t.currentTarget.dataset.type;
        switch (console.log("ssssss   ", t), e) {
          case "1":
            a.setData({
                taboption: 1
            });
            break;

          case "2":
            a.setData({
                taboption: 2
            });
            break;

          case "3":
            a.setData({
                taboption: 3
            });
        }
    },
    makePhoneCall: function() {
        wx.makePhoneCall({
            phoneNumber: this.data.shangjiaData.linkPhone
        });
    },
    gotomy: function() {
        wx.makePhoneCall({
            phoneNumber: this.data.shangjiaData.linkPhone
        });
    },
    toMap: function() {
        wx.openLocation({
            latitude: this.data.shangjiaData.latitude,
            longitude: this.data.shangjiaData.longitude,
            name: this.data.shangjiaData.name,
            address: this.data.shangjiaData.address
        });
    },
    onTabsItemTap: function(t) {
        var a = t.target.dataset.index;
        this.setData({
            currentTabsIndex: a
        });
    },
    getUserInfo: function() {
        this.setData({
            showPosterBox: !0
        }), wx.showLoading({
            title: "生成卡片中"
        }), e.creatPoster(this, "goodsCanvas", this.data.DetailsData.pic, this.data.DetailsData.name, this.data.DetailsData.minPrice, this.data.DetailsData.originalPrice, "抢购", "../../../common/images/posterTy.png", "", this.data.DetailsData.id);
    },
    closePosterBox: function() {
        this.setData({
            showPosterBox: !1
        });
    },
    toAddShopCar: function() {
        this.setData({
            shopType: "addShopCar",
            selectSizePrice: this.data.goodsDetail.basicInfo.minPrice
        }), this.bindGuiGeTap();
    },
    tobuy: function() {
        this.setData({
            shopType: "tobuy",
            selectSizePrice: this.data.goodsDetail.basicInfo.minPrice
        }), this.bindGuiGeTap();
    },
    numJianTap: function() {
        if (this.data.buyNumber > this.data.buyNumMin) {
            var t = this.data.buyNumber;
            t--, this.setData({
                buyNumber: t
            });
        }
    },
    numJiaTap: function() {
        if (this.data.buyNumber >= this.data.buyNumMax) wx.showToast({
            title: "已经最大库存数啦",
            icon: "none",
            duration: 2e3
        }); else {
            var t = this.data.buyNumber + 1;
            this.setData({
                buyNumber: t,
                finallyPrice: (t * this.data.price).toFixed(2)
            });
        }
    },
    labelItemTap: function(t) {
        for (var a = this, e = a.data.goodsDetail.properties[t.currentTarget.dataset.propertyindex].childsCurGoods, i = 0; i < e.length; i++) a.data.goodsDetail.properties[t.currentTarget.dataset.propertyindex].childsCurGoods[i].active = !1;
        a.data.goodsDetail.properties[t.currentTarget.dataset.propertyindex].childsCurGoods[t.currentTarget.dataset.propertychildindex].active = !0;
        for (var s = a.data.goodsDetail.properties.length, d = 0, n = "", r = "", i = 0; i < a.data.goodsDetail.properties.length; i++) {
            e = a.data.goodsDetail.properties[i].childsCurGoods;
            for (var c = 0; c < e.length; c++) e[c].active && (d++, n = n + a.data.goodsDetail.properties[i].id + ":" + e[c].id + ",", 
            a.data.goodsDetail.properties[t.currentTarget.dataset.propertyindex].optionValueId = e[c].id, 
            r = r + a.data.goodsDetail.properties[i].name + ":" + e[c].name + "  ");
        }
        var h = !1;
        s == d && (h = !0), h && wx.request({
            url: o.PubUrl + "/shop/goods/price",
            data: {
                goodsId: a.data.goodsDetail.basicInfo.id,
                propertyChildIds: n
            },
            success: function(t) {
                if (0 == t.data.code) {
                    var e = t.data.data;
                    a.setData({
                        selectSizePrice: e.price,
                        totalScoreToPay: e.score,
                        propertyChildIds: n,
                        propertyChildNames: r,
                        buyNumMax: e.stores,
                        buyNumber: e.stores > 0 ? 1 : 0
                    });
                }
            }
        }), this.setData({
            goodsDetail: a.data.goodsDetail,
            canSubmit: h
        });
    },
    addShopCar: function() {
        if (this.data.goodsDetail.properties && !this.data.canSubmit) return this.data.canSubmit || wx.showToast({
            title: "请选择商品规格",
            icon: "none"
        }), void this.bindGuiGeTap();
        if (this.data.buyNumber < 1) wx.showToast({
            title: "请选择购买数量",
            icon: "none"
        }); else {
            var t = this.bulidShopCarInfo();
            this.setData({
                shopCarInfo: t,
                shopNum: t.shopNum
            }), wx.setStorage({
                key: "shopCarInfo",
                data: t
            }), this.closePopupTap(), wx.showToast({
                title: "加入购物车成功",
                icon: "success",
                duration: 2e3
            });
        }
    },
    bulidShopCarInfo: function() {
        var t = {};
        t.goodsId = this.data.goodsDetail.basicInfo.id, t.pic = this.data.goodsDetail.basicInfo.pic, 
        t.name = this.data.goodsDetail.basicInfo.name, t.propertyChildIds = this.data.propertyChildIds, 
        t.label = this.data.propertyChildNames, t.price = this.data.selectSizePrice, t.score = this.data.totalScoreToPay, 
        t.left = "", t.active = !0, t.number = this.data.buyNumber, t.logisticsType = this.data.goodsDetail.basicInfo.logisticsId, 
        t.logistics = this.data.goodsDetail.logistics, t.weight = this.data.goodsDetail.basicInfo.weight;
        var a = this.data.shopCarInfo;
        console.log(a), a.shopNum || (a.shopNum = 0), a.shopList || (a.shopList = []);
        for (var e = -1, o = 0; o < a.shopList.length; o++) {
            var i = a.shopList[o];
            if (i.goodsId == t.goodsId && i.propertyChildIds == t.propertyChildIds) {
                e = o, t.number = t.number + i.number;
                break;
            }
        }
        return a.shopNum = a.shopNum + this.data.buyNumber, e > -1 ? a.shopList.splice(e, 1, t) : a.shopList.push(t), 
        a.kjId = this.data.kjId, a;
    },
    buyNow: function(t) {
        console.log("--------------------");
        var a = this, e = t.currentTarget.dataset.shoptype;
        if (this.data.goodsDetail.properties && !this.data.canSubmit) return this.data.canSubmit || wx.showToast({
            title: "请选择商品规格",
            icon: "none"
        });
        if (this.data.buyNumber < 1) wx.showModal({
            title: "请选择购买数量",
            icon: "none"
        }); else if (wx.getStorageSync("token")) {
            wx.showLoading();
            e = a.buy(e);
            wx.setStorage({
                key: "buyNowInfo",
                data: e
            }), console.log("数据", a.data), wx.navigateTo({
                url: "/pages/orderPay/index?orderType=buyNow"
            });
        } else console.log("你咋不登录呢"), wx.navigateTo({
            url: "/pages/login/index"
        });
    },
    buy: function() {
        var t = {};
        t.gooodsId = this.data.goodsDetail.basicInfo.id, t.pic = this.data.goodsDetail.basicInfo.pic, 
        t.name = this.data.goodsDetail.basicInfo.name, t.propertyChildIds = this.data.propertyChildIds, 
        t.label = this.data.propertyChildNames, t.price = this.data.selectSizePrice, t.score = this.data.totalScoreToPay, 
        t.left = "", t.active = !0, t.number = this.data.buyNumber, t.goodsId = this.data.id, 
        t.logisticsType = this.data.goodsDetail.basicInfo.logisticsId, t.logistics = this.data.goodsDetail.logistics, 
        t.propertyChildIds = this.data.propertyChildIds, t.weight = this.data.goodsDetail.basicInfo.weight;
        var a = {};
        return a.shopNum || (a.shopNum = 0), a.shopList || (a.shopList = []), a.shopList.push(t), 
        a;
    },
    closePay: function() {
        this.setData({
            payshow: !1
        });
    },
    savePostePic: function() {
        t = this.data.posterImg, wx.showLoading({
            title: "保存中..."
        });
        var t = wx.getStorageSync("posterPic_goodsCanvas_" + this.data.DetailsData.id);
        wx.saveImageToPhotosAlbum({
            filePath: t,
            success: function() {
                wx.showToast({
                    title: "保存成功！",
                    icon: "success",
                    duration: 1e3
                });
            },
            fail: function(t) {
                wx.showToast({
                    title: "保存失败！",
                    icon: "success",
                    duration: 1e3
                });
            }
        });
    },
    gotomap: function(t) {
        var a = t.currentTarget.dataset.lat, e = t.currentTarget.dataset.lng, o = t.currentTarget.dataset.addss, i = t.currentTarget.dataset.shopname;
        wx.navigateTo({
            url: "../map/map?lat=" + a + "&lng=" + e + "&addss=" + o + "&shopname=" + i
        });
    },
    onUnload: function() {
        wx.removeStorageSync("posterPic_goodsCanvas_" + this.data.DetailsData.id);
    },
    imgHeight: function(t) {
        var a = wx.getSystemInfoSync().windowWidth * t.detail.height / t.detail.width + "px", e = this.data.imgheights;
        e[t.target.dataset.id] = a, this.setData({
            imgheights: e
        });
    },
    bindchange: function(t) {
        this.setData({
            current: t.detail.current
        });
    }
});