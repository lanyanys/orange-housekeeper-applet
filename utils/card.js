var e = getApp();

module.exports = {
    creatPoster: function(t, l, i, s, a, n, o, f, c, d) {
        var r = wx.getSystemInfoSync(), x = r.windowWidth / 750 * 650, p = r.windowWidth / 750 * 1e3, w = wx.createCanvasContext(l);
        wx.downloadFile({
            url: i,
            success: function(f) {
                var r = f.tempFilePath;
                wx.request({
                    url: e.globalData.ApiUrl + "/xcx/code.php",
                    method: "POST",
                    header: {
                        "content-type": "application/x-www-form-urlencoded"
                    },
                    data: {
                        scene: d,
                        page: "pages/goodsDetail/index",
                        width: 200
                    },
                    success: function(e) {
                        wx.downloadFile({
                            url: e.data.data,
                            success: function(e) {
                                var f = e.tempFilePath;
                                w.setFillStyle("#fdd533"), w.fillRect(0, 0, x, p), w.setFillStyle("#ffffff"), w.fillRect(20, 20, .88 * x, .7 * p), 
                                w.drawImage(r, .09 * x, .1 * x, .8 * x, .55 * x), w.setFillStyle("black"), w.setShadow(0, 0, 0, "white"), 
                                w.setFontSize(parseInt(.05 * x)), w.setTextAlign("center");
                                var F = (i.shop_name ? "【" + i.shop_name + "】" : "") + s, h = F.length;
                                if (h > 16 ? (w.fillText(F.slice(0, 16), .4 * x, .75 * x, .8 * x), h > 36 ? w.fillText(F.slice(16, 32) + "...", .4 * x, .82 * x, .8 * x) : w.fillText(F.slice(16), .4 * x, .82 * x, .8 * x)) : w.fillText(F, .4 * x, .79 * x, .8 * x), 
                                a && (w.setFillStyle("#FF373E"), w.setFontSize(parseInt(.06 * x)), w.fillText("¥" + a, .5 * x, .93 * x)), 
                                "专题" == o && (w.setFillStyle("#FF363D"), w.setFontSize(parseInt(.07 * x)), w.fillText("超值优惠", .5 * x, .93 * x)), 
                                w.font = "oblique", w.setFillStyle("#A79E9F"), w.setFontSize(parseInt(.045 * x)), 
                                w.setTextBaseline("middle"), n && w.fillText("¥" + n, .5 * x, .98 * x), w.setFillStyle("#ffffff"), 
                                w.fillRect(20, .75 * p, .88 * x, 120), w.drawImage(f, .58 * x, 1.18 * x, .3 * x, .3 * x), 
                                w.setFillStyle("#757575"), w.setFontSize(parseInt(.03 * x)), w.fillText("发现一个好东西,想跟你分享", .4 * x, 1.42 * x), 
                                c) {
                                    var u = c.length;
                                    u > 16 ? (w.fillText("地址：" + c.slice(0, 16), .5 * x, .9 * x, .8 * x), u > 36 ? w.fillText(c.slice(16, 32) + "...", .5 * x, .95 * x, .8 * x) : w.fillText(c.slice(16), .5 * x, .95 * x, .8 * x)) : w.fillText("地址：" + c, .5 * x, .9 * x, .8 * x), 
                                    w.setFontSize(parseInt(.05 * x));
                                }
                                w.draw(!0), wx.hideLoading(), t.showPosterBox = !0, setTimeout(function() {
                                    wx.canvasToTempFilePath({
                                        x: 0,
                                        y: 0,
                                        width: x,
                                        height: p,
                                        destWidth: 650,
                                        destHeight: 1e3,
                                        canvasId: l,
                                        fileType: "jpg",
                                        quality: 1,
                                        success: function(e) {
                                            wx.setStorage({
                                                key: "posterPic_" + l + "_" + d,
                                                data: e.tempFilePath
                                            });
                                        },
                                        fail: function(e) {
                                            console.log(e);
                                        }
                                    });
                                }, 500);
                            }
                        });
                    },
                    error: function() {}
                });
            }
        });
    }
};