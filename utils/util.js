var t = function(t) {
    return (t = t.toString())[1] ? t : "0" + t;
};

module.exports = {
    formatTime: function(e) {
        var n = e.getFullYear(), o = e.getMonth() + 1, r = e.getDate(), u = e.getHours(), i = e.getMinutes(), s = e.getSeconds();
        return [ n, o, r ].map(t).join("/") + " " + [ u, i, s ].map(t).join(":");
    }
}, module.exports = {
    checkPhone: function(t) {
        return !!/^1[3456789]\d{9}$/.test(t);
    }
};