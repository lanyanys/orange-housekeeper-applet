App({
    onLaunch: function() {
        var a = this;
        wx.request({
            url: a.globalData.PubUrl + "/config/value",
            data: {
                key: "mallName"
            },
            success: function(a) {
                console.log(a.data.data), 0 == a.data.code ? wx.setStorageSync("mallName", a.data.data) : wx.setStorageSync("mallName", "首页");
            }
        });
    },
    isLogin: function(a) {
        console.log("login");
        var e = this;
        a ? wx.request({
            url: e.globalData.PubUrl + "/user/check-token",
            data: {
                token: a
            },
            success: function(a) {
                0 != a.data.code && (wx.removeStorageSync("token"), e.goLoginPageTimeOut());
            }
        }) : e.goLoginPageTimeOut();
    },
    goLoginPageTimeOut: function() {
        setTimeout(function() {
            wx.navigateTo({
                url: "/pages/login/index"
            });
        }, 1e3);
    },
    globalData: {
        token: "",
        userInfo: null,
        subDomain: "bycs",
        PubUrl: "https://api.it120.cc/bycs/",
        ApiUrl: "https://code.if21.cn/",
        version: "1.0",
        shareProfile: "橙色管家",
        networkFail: "温馨提示：数据丢失了"
    }
});